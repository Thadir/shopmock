package net.thadir.jdriven.searchproducts;

import io.micronaut.runtime.Micronaut;

public class SearchProductApplication {

  public static void main(String[] args) {
    Micronaut.run(SearchProductApplication.class);
  }
}
