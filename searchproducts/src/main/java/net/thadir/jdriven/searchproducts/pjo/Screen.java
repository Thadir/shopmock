package net.thadir.jdriven.searchproducts.pjo;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@JsonAutoDetect
@AllArgsConstructor
@NoArgsConstructor
public class Screen {
  String        producode;
  String        schermdiagonaal;
  String        resolutie;
  String        beeldverhouding;
  String        videoIn;
  String        schermkromming;
  String        beeldschermcoating;
  double      price;
  LocalDateTime updated;
}
