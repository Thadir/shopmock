package net.thadir.jdriven.searchproducts;

import io.micronaut.http.HttpRequest;
import io.micronaut.http.client.HttpClient;
import net.thadir.jdriven.searchproducts.pjo.Screen;

import java.net.MalformedURLException;
import java.net.URL;

public class DataRetriever {

  public static Screen[] getExternalData() throws MalformedURLException {
    URL uri = new URL("http://external-mock-api:8080");
    HttpClient client = HttpClient.create(new URL("http://external-mock-api:8080"));
    Screen[] screens = client.toBlocking().retrieve(HttpRequest.GET("/screens"), Screen[].class);
    return screens;
  }
}
