package net.thadir.jdriven.searchproducts;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import net.thadir.jdriven.searchproducts.pjo.Screen;

import javax.validation.constraints.NotNull;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller("/search/")
public class SearchServiceControler {

  @Get(uri = "/{filterString}")
  public List<Screen> getScreens(@NotNull String filterString) throws MalformedURLException {
    List<Screen> toFileter = Arrays.asList(DataRetriever.getExternalData());

    return toFileter.stream().filter(
        screen -> screen.getBeeldschermcoating().contains(filterString) || screen.getBeeldverhouding().contains(filterString) || screen.getProducode()
            .contains(filterString) || screen.getResolutie().contains(filterString) || screen.getSchermdiagonaal().contains(filterString) || screen
            .getSchermkromming().contains(filterString) || screen.getVideoIn().contains(filterString)).collect(Collectors.toList());
  }
}