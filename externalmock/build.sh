#!/bin/bash
#Setting fail fast for script.
set -e

echo "Building a cleanmock class."
mvn clean install

echo "Re building docker."
docker-compose build --force