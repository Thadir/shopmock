package net.thadir.jdriven.externalmock;

import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import net.thadir.jdriven.externalmock.dto.Screen;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Controller("/")
public class ExternalServiceControler {

  @Get(uri = "/screens/", produces = MediaType.APPLICATION_JSON)
  public List<Screen> getScreens() {
    List<Screen> result = new ArrayList<>();
    result.add(Screen.builder().producode("aocc32g1").price(199.00).updated(LocalDateTime.now()).schermdiagonaal("31,5\"").resolutie("1920x1080 (Full HD)").build());
    result.add(Screen.builder().producode("samsunglc32jg50qqu").price(299.00).updated(LocalDateTime.now()).schermdiagonaal("31,5\"").resolutie("2560x1440 (Quad HD)").build());
    return result;
  }
}