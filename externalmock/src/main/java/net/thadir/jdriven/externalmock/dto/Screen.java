package net.thadir.jdriven.externalmock.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
public class Screen {
  String        producode;
  String        schermdiagonaal;
  String        resolutie;
  String        beeldverhouding;
  String        videoIn;
  String        schermkromming;
  String        beeldschermcoating;
  double      price;
  LocalDateTime updated;
}
