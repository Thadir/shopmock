package net.thadir.jdriven.externalmock;

import io.micronaut.runtime.Micronaut;

public class ExternalServerMockApplication {

  public static void main(String[] args) {
    Micronaut.run(ExternalServerMockApplication.class);
  }
}
